module.exports = (io) => {
    io.on('connection', (socket) => {
        console.log('User connected. Socket id %s', socket.id);

        socket.on('newProduct', (data) => {
            socket.broadcast.emit('newProduct', data);
        });

        socket.on('deleteProduct', (data) => {
            io.emit('deleteProduct', data);
        });

        socket.on('disconnect', () => {
            console.log('User disconnected. Socket id %s', socket.id);
        });
    });
};
