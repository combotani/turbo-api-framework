const express = require('express');
const path = require('path');
const logger = require('morgan');
const mongoose = require('mongoose');

const authentication = require('./api/middlewares/authentication');

const addRoute = require('./api/cores/routes');
const userRouter = require('./api/controllers/users');
const pushRouter = require('./api/controllers/pushes');

mongoose.connect('mongodb://localhost:27017/turbo', { useNewUrlParser: true })
    .then(() => console.log('Connected to MongoDB...'))
    .catch(ex => console.log('Could not connect to MongoDB...'));

const app = express();

// app.use(logger('dev'));
app.use(express.json());
app.use(require('./api/middlewares/cors'));

app.use('/api/users', userRouter);
app.use('/api/pushes', pushRouter);

// app.use(authentication);
addRoute(app);

module.exports = app;