const webPush = require('web-push');

module.exports = (subscription, payload) => {
    let vapidPublicKey = 'BDa8YzuwhIZGSqOr77I9jIX-xADwHWMFcVASWFFnxbinF6ZPFWVE83kOMb1WJI4FwJ1_MPCpR572bNI0t0fdbKg';
    let vapidPrivateKey = 'og9z_t8i59VKBDfrjZeqvxSYRpYxij88rQWhjXzxG1s';
  
    var payload = payload;
  
    var options = {
      // gcmAPIKey: 'YOUR_SERVER_KEY',
      TTL: 60,
      vapidDetails: {
        subject: 'mailto:YOUR_EMAIL_ADDRESS',
        publicKey: vapidPublicKey,
        privateKey: vapidPrivateKey
      }
    };

    subscription.forEach(async function(i) {
      var pushSubscription = i;
      webPush.sendNotification(
        pushSubscription,
        payload,
        options
      );
    });
}