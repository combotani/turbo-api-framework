import React, { Component } from 'react';
import ProductList from './ProductList';
import AddProduct from './AddProduct';

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productName: '',
      quantity: 0,
      products: [],
    }
  }

  onDeleteBtn = (data) => {    
    this.props.socket.emit('deleteProduct', data);
    fetch(`http://localhost:5000/api/products/${data._id}`, {
      method: 'DELETE',
      headers:{
        'Content-Type': 'application/json'
      }
    })
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit = e => {
    e.preventDefault();
    const product = {
      productName: this.state.productName,
      quantity: this.state.quantity
    };
    this.props.socket.emit('newProduct', product);

    fetch(`http://localhost:5000/api/products`, {
      method: 'POST',
      body: JSON.stringify(product),
      headers:{
        'Content-Type': 'application/json'
      }
    })
    .then(response => response.json())
    .then(product => this.setState({ products: [...this.state.products, product] }));
  }

  getProduct() {
    fetch('http://localhost:5000/api/products')
      .then(response => response.json())
      .then(products => this.setState({ products: products }));
  }

  componentDidMount() {
    this.getProduct();
  }

  componentWillMount() {
    this.props.socket.on('deleteProduct', (data) => {
      const arr = [...this.state.products];
      const index = arr.findIndex(x => x._id === data._id);
      arr.splice(index, 1);
      this.setState({ products: arr });
    });
  }

  render() {
    this.props.socket.on('newProduct', () => this.getProduct());
    return (
      <div className="row">
          <AddProduct handleChange={this.handleChange} handleSubmit={this.handleSubmit} />
          <ProductList products={this.state.products} onDeleteBtn={this.onDeleteBtn} />
      </div>
    );
  }
}

export default Product;