import React, { Component } from 'react';
import Product from './Product';
import io from 'socket.io-client';

const socket = io('http://127.0.0.1:5000');

class App extends Component {
  render() {
    return (
        <Product socket={socket}/>
    );
  }
}

export default App;
