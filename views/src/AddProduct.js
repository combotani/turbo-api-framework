import React  from 'react';

const AddProduct = ({handleSubmit, handleChange}) => {
  return (
    <div className="col-md-6">
      <form onSubmit={ handleSubmit }>
        <div className="row">
          <div className="form-group col-md-8">
            <label htmlFor="productName">Product Name</label>
            <input type="text" className="form-control" name="productName" autoComplete="off" onChange={ handleChange } />
          </div>
          <div className="form-group col-md-4">
            <label htmlFor="quantity">Quantity</label>
            <div className="input-group mb-3">
              <input type="text" className="form-control" name="quantity" autoComplete="off" onChange={ handleChange } />
              <div className="input-group-append">
                <button className="btn btn-outline-primary" type="submit">Add</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}

export default AddProduct;