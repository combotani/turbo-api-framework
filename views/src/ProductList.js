import React  from 'react';

const ProductList = ({ products, onDeleteBtn }) => {
  console.log(products);
  return (
    <div className="col-md-6">
      <div className="p-3 mb-2 bg-light text-dark">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Product Name</th>
              <th scope="col">QTY</th>
              <th scope="col">Handle</th>
            </tr>
          </thead>
          <tbody>
            { products.map((product, i) => 
                <tr key={i}>
                  <th>{i+1}</th>
                  <td>{product.productName}</td>
                  <td>{product.quantity}</td>
                  <td>
                    <button className="btn btn-danger" onClick={onDeleteBtn.bind(this, product)}>Delete</button>
                  </td>
                </tr>
              )}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default ProductList;