const router = require('express').Router();
const bcrypt = require('bcrypt');

const authentication = require('../middlewares/authentication');
const role = require('../middlewares/roles');
const User = require('../models/user');

router.get('/me', authentication, async (req, res) => {
    try { 
        let user = await User.findById(req.user._id);
        res.json(user);
    } catch(ex) {
        return res.status(500).json({ message: ex.message });
    }
});

router.post('/login', async (req, res) => {
    let user = await User.findOne({ email: req.body.email });
    if(!user) return res.status(400).json({ message: "There was a problem with your login." });

    let password = await bcrypt.compare(req.body.password, user.password);
    if(!password) return res.status(400).json({ message: "There was a problem with your login." });

    let token = user.genToken();
    res.send(token);
});

router.post('/signup', async (req, res) => {
    let user = new User(req.body);

    try {
        let doc = await user.save();
        res.json(doc);
    } catch(ex) {
        return res.status(405).json({ message: ex.message });
    }
});

router.get('/', authentication, role('admin'), async (req, res) => {
    try {
        let docs = await User.find();
        res.json(docs);
    } catch(ex) {
        return res.status(500).json({ message: ex.message });
    }
});

// router.put('/', async (req, res) => {
//     res.send(req.method + " " + req.originalUrl);
// });

// router.delete('/', async (req, res) => {
//     res.send(req.method + " " + req.originalUrl);
// });

module.exports = router;