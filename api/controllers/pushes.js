const router = require('express').Router();
const dns = require('dns');

const push = require('../../notifications/main');
const authentication = require('../middlewares/authentication');
const role = require('../middlewares/roles');
const Subscription = require('../models/subscription');

router.post('/subscribe', async (req, res) => {
    res.status(201).json({});

    try {
        let subscription = new Subscription(req.body);
        let doc = await subscription.save();
    } catch(ex) {
        return console.log(ex.message);
    }
});

router.post('/unsubscribe', async (req, res) => {
    res.status(201).json({});

    try {
        let doc = await Subscription.findOneAndRemove(req.body);
    } catch(ex) {
        return console.log(ex.message);
    }
});

router.post('/message', authentication, role('admin'), async (req, res) => {
    let data = {
        title: req.body.title,
        options: {
            body: req.body.payload,
            icon: 'images/icon.png',
            badge: 'images/badge.png',
            vibrate: [100, 50, 100],
            tag: 'id2'
        }
    };
    
    dns.lookup('www.google.com', async (err) => {
        if(err) {
            res.status(504).json({ message: "No connection." });
        }
        else {
            try {
                let subscription = await Subscription.find();
                if(subscription.length <= 0) return res.json({ message: "There're no subscription." });
                push(subscription, JSON.stringify(data));
                res.json({ message: "Notification pushed." });
        
            } catch(ex) {
                console.log(ex.message);
            }
        }
    });
});

module.exports = router;