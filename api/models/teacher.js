const mongoose = require('mongoose');
const omit = require('./functions/fnOmit');

const teacherSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    sex: {
        type: String,
        required: true
    }
});

omit(teacherSchema, ['__v']);

module.exports = mongoose.model('Teacher', teacherSchema);