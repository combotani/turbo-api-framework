const mongoose = require('mongoose');

guestSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Guest', guestSchema);