const mongoose = require('mongoose');
const omit = require('./functions/fnOmit');

const studentSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    sex: {
        type: String,
        required: true
    }
});

omit(studentSchema, ['__v']);

module.exports = mongoose.model('Student', studentSchema);