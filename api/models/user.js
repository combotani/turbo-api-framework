const mongoose = require('mongoose');
require('mongoose-type-email');
const omit = require('./functions/fnOmit');
const hash = require('./functions/fnHash');
const token = require('./functions/fnToken');

const userSchema = new mongoose.Schema({
    email: {
        type: mongoose.SchemaTypes.Email,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true
    }
});

hash(userSchema);

token(userSchema);

omit(userSchema, ['__v', 'password']);

module.exports = mongoose.model('User', userSchema);