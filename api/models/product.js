const mongoose = require('mongoose');
const omit = require('./functions/fnOmit');

const productSchema = new mongoose.Schema({
    productName: {
        type: String,
        required: true
    },
    quantity: {
        type: String,
        required: true
    }
});

omit(productSchema, ['__v']);

module.exports = mongoose.model('Product', productSchema);