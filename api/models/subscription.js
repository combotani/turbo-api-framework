const mongoose = require('mongoose');
const omit = require('./functions/fnOmit');

const subscriptionSchema = new mongoose.Schema({
    endpoint: {
        type: String,
        unique: true
    },
    expirationTime: Object,
    keys: {
        p256dh: String,
        auth: String
    }
});

omit(subscriptionSchema, ['__v']);

module.exports = mongoose.model('Subscription', subscriptionSchema);