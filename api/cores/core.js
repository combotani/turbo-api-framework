const router = require('express').Router();

const role = require('../middlewares/roles');

module.exports = (endPoint, model, callback) => {

    let Model = require(`../models/${model}`);

    router.get(endPoint, async (req, res) => {
        try {
            let docs = await Model.find();
            res.json(docs);
        } catch(ex) {
            return res.status(500).json({ message: ex.message });
        }
    });

    router.post(endPoint, async (req, res) => {
        let model = new Model(req.body);

        try {
            let doc = await model.save();
            res.json(doc);
        } catch(ex) {
            return res.status(405).json({ message: ex.message });
        }
    });

    router.put(endPoint + '/:id', role('owner', 'admin'), async (req, res) => {
        try {
            let doc = await Model.findByIdAndUpdate(req.params.id, { $set: req.body });
            if(!doc) return res.status(400).json({ message: "ID Not Found" });
            res.json(doc);
        } catch(ex) {
            return res.status(405).json({ message: ex.message });
        }
    });

    router.delete(endPoint + '/:id', async (req, res) => {
        try { 
            let doc = await Model.findByIdAndRemove(req.params.id);
            if(!doc) return res.status(400).json({ message: "ID Not Found" });
            res.json(doc);
        } catch(ex) {
            return res.status(405).json({ message: ex.message });
        }
    });

    router.get(endPoint + '/:id', async (req, res) => {
        try { 
            let doc = await Model.findById(req.params.id);
            if(!doc) return res.status(400).json({ message: "ID Not Found" });
            res.json(doc);
        } catch(ex) {
            return res.status(405).json({ message: ex.message });
        }
    });

    callback(router);
}