const core = require('./core');
const data = require('./config.json')

module.exports = (app) => {
    for(let i=0; i<data.length; i++) {
        core(data[i].endPoint, data[i].model, function(router) {
            app.use('/api', router);
        });
    }
}