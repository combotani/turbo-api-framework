const http = require('http');
const app = require('./app');

var port = process.env.PORT || 5000;

const server = http.createServer((app));

// Socket.IO
const io = require('socket.io').listen(server);
require('./sockets/base')(io);

server.listen(port, () => console.log(`Server started on port ${port}`))